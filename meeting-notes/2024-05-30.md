# bootable containers initiative meeting - 2024-05-30

## Attendees

- Colin Walters
- Hristo Marinov
- Micah Abbott
- Paul Whalen
- Robert Sturla
- Jason Brooks
- Eric Curtin
- Liora Milbaum
- Timothee Ravier
- Joseph Marrero

## Agenda

- Topics tagged with "Meeting": https://gitlab.com/fedora/bootc/tracker/-/issues/?label_name%5B%5D=Meeting

### Recording Meetings

- https://gitlab.com/fedora/bootc/tracker/-/issues/10
- Jason Brooks to investigate options, maybe upload to Fedora YouTube channel
- Experimenting with Google Meet transcription for this meeting
- Looking for lowest barrier of entry

### Dedicated build/test tooling?

- https://gitlab.com/fedora/bootc/tracker/-/issues/2
- Colin Walters proposed "tiers" of functionality that would exist in the tool
- Want to be able to use a container i.e. quay.io/fedora/bootc-sdk
- No opposition to the general idea, needs more discussion

### Road to Fedora 41

- https://gitlab.com/fedora/bootc/tracker/-/issues/11
- Feels like there are a lot of issues to uncover and tackle; needs more discovery and tracking of follow-up work
- Use of `composefs` was discussed and the problems that introduces with `grub`
- `bootupd` will almost be a requirement for systems using `bootc`
  - https://fedoraproject.org/wiki/Changes/FedoraSilverblueBootupd
  - https://gitlab.com/fedora/ostree/sig/-/issues/1
- `bootc` 1.0 milestone is tracked
  - https://github.com/containers/bootc/issues?q=is%3Aopen+is%3Aissue+milestone%3A1.0
