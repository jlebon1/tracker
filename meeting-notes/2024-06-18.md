# bootable containers initiative meeting - 2024-06-18

## Attendees

- Timothée Ravier
- Robert Sturla
- Hristo Marinov
- Joseph Marrero
- Peter Robinson
- Paul Whalen
- Colin Walters
- Noel Miller

## Agenda

## Decide who will be writting notes and who will make the meeting notes PR

- Joseph

## SELinux Labeling

- https://github.com/ostreedev/ostree-rs-ext/issues/388
- RS: plans to resolve this issue?
- https://codeberg.org/OpenVPN/openvpn3-linux/issues/7
- CW: https://github.com/containers/bootc/issues/571
- https://github.com/ostreedev/ostree-rs-ext/issues/510
- They are part of the top of mind issues.
- TR: This is also needed for composefs signing for derived containers
- Also related to https://github.com/containers/buildah/issues/5592

## https://gitlab.com/fedora/bootc/tracker/-/issues/3

- TR: atomic desktops trying to do a hackish workaround, but did not work.
- maybe fix during post-process?
- CW: https://github.com/ostreedev/ostree/issues/1469
- We want to generalize reproducible builds in containers, and teach the tools to canonalize the timestamps to 0.
- Long term we want to use composefs for the data model and not ostree which will solve the issue.
- A possible blocker for this would be Grub2 failing to work with composefs.
- TR: https://fedoraproject.org/wiki/Changes/ComposefsAtomicCoreOSIoT
- proposal to unify every variant on composefs

## composefs in Fedora 41

- In progress
- https://fedoraproject.org/wiki/Changes/ComposefsAtomicCoreOSIoT

## https://gitlab.com/fedora/bootc/tracker/-/issues/7

- "Small" / scoped issue for folks that want to get started contributing

## https://gitlab.com/fedora/bootc/tracker/-/issues/11

- DNF5/bootc change accepted
- IoT roadmap coming soon

## https://gitlab.com/-/snippets/3716947

- Will be copied into a more general roadmap issue
