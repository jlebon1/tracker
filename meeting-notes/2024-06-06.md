# bootable containers initiative meeting - 2024-06-06

## Attendees

- Jason Brooks
- Hristo Marinov
- Paul Whalen
- Colin Walters
- John Eckersberg
- Timothée Ravier
- Robert Sturla

## Agenda

## Decide who will be writting notes and who will make the meeting notes PR

- jbrooks will do it today

## Roadmap for Fedora 41

- deadlines for F41 changes: https://fedorapeople.org/groups/schedule/f-41/f-41-key-tasks.html

- composefs aligment:
- bootc images have composefs enabled by default
- will need a change for f41
- https://gitlab.com/fedora/bootc/tracker/-/issues/11#note_1929130136
- https://github.com/coreos/fedora-coreos-tracker/issues/1718
- https://gitlab.com/fedora/ostree/sig/-/issues/35
- need for a blog post to explain the importance of this feature
- makes the system harder to break
- improves on disk integrity

- Noel to reach out to anaconda folks to come to a bootc meeting to discuss

- jbrooks to make list of feature areas w/ point person for each

- Queue up some good first issues for different feature/component areas

- need for release cadence and notes to focus attention on what's happening, where to get involved
roll up bundle of release notes for podman-desktop-plugin-bootc, bootc-image-builder, base images

## CI and bootc

- Paul will follow up, Fedora IoT isn't currently integrated w/ CI
